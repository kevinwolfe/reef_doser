pushd "%~dp0"
openocd -f ./jlink.cfg -f ./esp32s2.cfg -c "program_esp ../build/mqtt_tcp.bin 0x10000 exit"
openocd -f ./jlink.cfg -f ./esp32s2.cfg -c "program_esp empty.bin 0x100000 reset exit"
popd

//file: main.c or main.cpp
#include "Arduino.h"

#include <WiFi.h>
#include <WiFiClient.h>
#include <WebServer.h>
#include <ElegantOTA.h>

#include "esp_system.h"
#include "nvs_flash.h"

#include "mqtt_client.h"

const char* ssid = "PlaidCarpet";
const char* password = "XXXXXXXXXXX";

WebServer server(80);
bool mqtt_subscribed = false;
esp_mqtt_client_handle_t mqtt_client;
bool dosing_in_progress = false;

uint32_t dosing_start_time_s = 0;
uint32_t dosing_stop_time_s  = 0;
uint64_t total_dosing_time_ms  = 0;

//-----------------------------------------------------------------------------
enum
{
  NVM_PARAMETER_RESET_COUNTER,
  NVM_PARAMETER_DOSER_RUN_TIME_S,
  NVM_PARAMETER_DOSER_RUN_PERIOD_S,
  NVM_PARAMETER_COUNT,
};

typedef struct
{
  const char  *p_name;
  int32_t    value;
  int32_t    default_value;
} nvm_parameter_t;

static nvm_parameter_t nvm_params[] = 
{
  [NVM_PARAMETER_RESET_COUNTER]      = { .p_name = "reset_counter",   .value = -1, .default_value = 0 },
  [NVM_PARAMETER_DOSER_RUN_TIME_S]   = { .p_name = "doser_runtime_s", .value = -1, .default_value = 0 },
  [NVM_PARAMETER_DOSER_RUN_PERIOD_S] = { .p_name = "doser_period_s",  .value = -1, .default_value = 0 },
};

bool nvm_params_updated = false;

//-----------------------------------------------------------------------------
void update_nvm_parameters();
void parse_set_params_request( char *p_msg );
void mqtt_event_handler(void *handler_args, esp_event_base_t base, int32_t event_id, void *event_data);
uint32_t system_uptime_s( void );
void send_mqtt_status_msg( esp_mqtt_client_handle_t *p_client, bool doser_running );

//-----------------------------------------------------------------------------
void parse_set_params_request( char *p_msg )
{
  Serial.printf("Parsing set params message: %s\n", p_msg );
  while ( *p_msg )
  {
    if ( *p_msg == '\"' )
    {
      p_msg++;
      char *p_name_start = p_msg;
      uint8_t name_str_len = 0;
      uint8_t value_str_len = 0;
      
      while ( *p_msg && ( *p_msg != '\"' ))
      {
        p_msg++;
        name_str_len++;
      }
      
      // Only accept positive numbers for values
      while ( *p_msg && ( *p_msg < '0' || *p_msg > '9' ) )
      {
        p_msg++;
      }

      char *p_value_start = p_msg;      
      while ( *p_msg && ( *p_msg >= '0' && *p_msg <= '9' ) )
      {
        p_msg++;
        value_str_len++;
      }
      
      p_name_start[name_str_len] = 0;
      p_value_start[value_str_len] = 0;
      Serial.printf( "... PARAM: *%s*, value: *%s*\n", p_name_start, p_value_start );
      
      for ( uint8_t idx = 0; idx < NVM_PARAMETER_COUNT; idx++ )
      {
        if ( !strcmp( nvm_params[idx].p_name, p_name_start ) && strlen( p_value_start ) )
        {
          nvm_params[idx].value = atoi( p_value_start );
          nvm_params_updated = true;
        }
      }
      
    }
    p_msg++;
  }
}

//-----------------------------------------------------------------------------
void mqtt_event_handler(void *handler_args, esp_event_base_t base, int32_t event_id, void *event_data)
{
    esp_mqtt_event_handle_t event = ( esp_mqtt_event_handle_t )event_data;
    esp_mqtt_client_handle_t client = event->client;
    switch ((esp_mqtt_event_id_t)event_id)
    {
      case MQTT_EVENT_CONNECTED:
          Serial.printf( "MQTT Connected to server\n" );
          esp_mqtt_client_subscribe(client, "reef/doser/set_params", 0);
          break;

      case MQTT_EVENT_DISCONNECTED:
          mqtt_subscribed = false;
          Serial.printf( "MQTT Server Disconnect!\n");
          break;

      case MQTT_EVENT_DATA:
          event->data[event->data_len] = 0;
          parse_set_params_request( event->data );
          break;

      case MQTT_EVENT_ERROR:
          Serial.printf( "MQTT Event Error!\n" );
          break;

      case MQTT_EVENT_SUBSCRIBED:
        mqtt_subscribed = true;
        break;
        
      case MQTT_EVENT_UNSUBSCRIBED:
        mqtt_subscribed = false;
        break;
      
      case MQTT_EVENT_PUBLISHED:
      default:
        break;
    }
    fflush(stdout);
}

//-----------------------------------------------------------------------------
void update_nvm_parameters()
{
  // Initialize NVS
  static bool first_pass = true;
  esp_err_t err;
  if ( first_pass )
  {
    err = nvs_flash_init();
    if (err == ESP_ERR_NVS_NO_FREE_PAGES)
    {
        nvs_flash_erase();
        nvs_flash_init();
    }
  }

  nvs_handle flash_handle;
  err = nvs_open( "storage", NVS_READWRITE, &flash_handle );
  if (err != ESP_OK)
  {
      Serial.printf( "Error (%d) opening NVS handle!\n", err);
  }
  else
  {
    bool table_dirty = false;

    for ( uint8_t idx = 0; idx < NVM_PARAMETER_COUNT; idx++ )
    {
      bool write_value = false;
      int32_t temp_read_val;
      if ( ESP_OK == nvs_get_i32(flash_handle, nvm_params[idx].p_name, &temp_read_val) )
      {
        if ( first_pass )
        {
          nvm_params[idx].value = temp_read_val;
          Serial.printf( "Loaded NVM Param '%s': %i\n", nvm_params[idx].p_name, nvm_params[idx].value );
        }
        else if ( nvm_params[idx].value != temp_read_val )
        {
          Serial.printf( "Updating NVM Param '%s' from %i to %i\n", nvm_params[idx].p_name, temp_read_val, nvm_params[idx].value );
          write_value = true;
        }
      }
      else
      {
        Serial.printf( "Error reading NVM Param: '%s', loading default\n", nvm_params[idx].p_name );
        nvm_params[idx].value = nvm_params[idx].default_value;
        write_value = true;
      }
      if (write_value)
      {
        nvs_set_i32(flash_handle, nvm_params[idx].p_name, nvm_params[idx].value );
        table_dirty = true;
      }
    }
    
    if ( table_dirty )
    {
      if ( ESP_OK != nvs_commit(flash_handle) )
      {
        Serial.printf("Error writing\n" );
      }
    }

    nvs_close(flash_handle);
  }
  first_pass = false;
}

//-----------------------------------------------------------------------------
static void wifi_task( void *Param )
{
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);

  server.on("/", []() 
  {
    char root_http[512];
    sprintf( root_http, "<html><body>Dosing Pump.  Click <a href=\"http://%s/update\">here</a> to update firmware</body></html>", WiFi.localIP().toString().c_str());    
    server.send(200, "text/html", (String)root_http);
  });

  ElegantOTA.begin(&server);    // Start ElegantOTA
  server.begin();
  
  esp_mqtt_client_config_t mqtt_cfg;
  memset( &mqtt_cfg, 0, sizeof( mqtt_cfg ) );
  mqtt_cfg.uri = "mqtt://192.168.0.41";

  mqtt_client = esp_mqtt_client_init(&mqtt_cfg);
  esp_mqtt_client_register_event(mqtt_client, (esp_mqtt_event_id_t)ESP_EVENT_ANY_ID, mqtt_event_handler, NULL);
  esp_mqtt_client_start(mqtt_client);
  
  const uint32_t task_delay_ms = 10;
  
  uint32_t unconnected_timer_ms = 0;
  while(1)
  {    
    vTaskDelay( task_delay_ms / portTICK_RATE_MS);
    uint32_t now_s = system_uptime_s();
    
    // Wait for connection
    if ( WiFi.status() != WL_CONNECTED )
    {
      unconnected_timer_ms += task_delay_ms;
      if ( unconnected_timer_ms > 20 * 1000 )
      {
        WiFi.begin( ssid, password );
        unconnected_timer_ms = 0;
      }
      continue;
    }
    
    // We're connected!
    server.handleClient();
    
    static uint32_t time_since_last_update_ms = 0;
    static bool     dosing_in_progress_last = false;

    time_since_last_update_ms += task_delay_ms;
    if ( ( time_since_last_update_ms > 5000 ) || ( dosing_in_progress != dosing_in_progress_last ) )
    {
      printf("%i: Sending MQTT update\n", now_s );
      send_mqtt_status_msg( &mqtt_client, dosing_in_progress );
      time_since_last_update_ms = 0;
      dosing_in_progress_last = dosing_in_progress;
    }
  }
}

//-----------------------------------------------------------------------------
uint32_t system_uptime_s( void )
{
  // esp_timer_get_time returns time in uSec
  return esp_timer_get_time() / 1000000;
}

//-----------------------------------------------------------------------------
void send_mqtt_status_msg( esp_mqtt_client_handle_t *p_client, bool doser_running )
{
  static char status_msg[512];
  static uint32_t status_msg_id = 0;

  char *p_msg = status_msg;
  status_msg_id++;
  p_msg += sprintf( p_msg, "{ \"message_id\":%i,", status_msg_id );

  for ( uint8_t idx = 0; idx < NVM_PARAMETER_COUNT; idx++ )
  {
    p_msg += sprintf( p_msg, "\"%s\":%i,", nvm_params[idx].p_name, nvm_params[idx].value );
  }

  p_msg += sprintf( p_msg, "\"ip_addr\":\"%s\",",         WiFi.localIP().toString().c_str() );
  p_msg += sprintf( p_msg, "\"current_time\":%u,",    system_uptime_s() );
  p_msg += sprintf( p_msg, "\"dose_start_time\":%u,", dosing_start_time_s );
  p_msg += sprintf( p_msg, "\"dose_stop_time\":%u,",  dosing_stop_time_s );
  p_msg += sprintf( p_msg, "\"total_dose_time\":%u,", (uint32_t)( total_dosing_time_ms / 1000  ) );

  p_msg += sprintf( p_msg, "\"doser_running\":\"%s\"}", doser_running ? "True" : "False" );

  esp_mqtt_client_publish( *p_client, "reef/doser/status", status_msg, 0, 1, 0);      
}


//-----------------------------------------------------------------------------
extern "C" void app_main()
{
  initArduino();
  Serial.begin(115200);

  Serial.printf( "Reef Doser app init\n");
  
  update_nvm_parameters();
  
  nvm_params[NVM_PARAMETER_RESET_COUNTER].value++;
  nvm_params_updated = true;
  
  // Put all the wifi stuff in a separate task so that we don't have to wait for a connection
  xTaskCreate( wifi_task, "app_task", 4096, NULL, 0, NULL );
  
#define DOSER_DRIVE    (gpio_num_t)( 3 )
  gpio_config_t io_conf;
  io_conf.pin_bit_mask = ( 1ULL<< (uint8_t)DOSER_DRIVE );
  io_conf.mode = GPIO_MODE_OUTPUT;
  gpio_set_level( DOSER_DRIVE, 0 );
  gpio_config(&io_conf);

#define USER_BUTTON    (gpio_num_t)( 2 )
  io_conf.pin_bit_mask = ( 1ULL<< (uint8_t)USER_BUTTON );
  io_conf.mode = GPIO_MODE_INPUT;
  gpio_set_level( USER_BUTTON, 1 );
  gpio_config(&io_conf);
  
  const uint32_t task_delay_ms = 100;
  
  dosing_start_time_s = system_uptime_s() + nvm_params[NVM_PARAMETER_DOSER_RUN_TIME_S].value;
  dosing_stop_time_s  = 0;
  
  while ( 1 )
  {
    vTaskDelay( task_delay_ms / portTICK_RATE_MS);
    if ( dosing_in_progress )
    {
      total_dosing_time_ms += task_delay_ms;
    }
    
    uint32_t now_s = system_uptime_s();
    
    if ( nvm_params[NVM_PARAMETER_DOSER_RUN_TIME_S].value && nvm_params[NVM_PARAMETER_DOSER_RUN_PERIOD_S].value )
    {
      if ( !dosing_in_progress && ( now_s > dosing_start_time_s ) )
      {
        printf( "%i: Running dosing pump for %i seconds\n", now_s, nvm_params[NVM_PARAMETER_DOSER_RUN_TIME_S].value );
        dosing_in_progress = true;
        gpio_set_level( DOSER_DRIVE, 1 );
        dosing_stop_time_s = dosing_start_time_s + nvm_params[NVM_PARAMETER_DOSER_RUN_TIME_S].value;
      }
      
      if ( dosing_in_progress && ( now_s > dosing_stop_time_s ) )
      {
        printf( "%i: Stopping dosing pump\n", now_s );
        dosing_in_progress = false;
        gpio_set_level( DOSER_DRIVE, 0 );
        dosing_start_time_s = dosing_start_time_s + nvm_params[NVM_PARAMETER_DOSER_RUN_PERIOD_S].value;
      }
    }

    static bool button_pressed_last;
    bool button_pressed = !gpio_get_level(USER_BUTTON);
    if ( button_pressed != button_pressed_last )
    {
      printf( "%06i: Button state change!: %i\n", now_s, button_pressed );
      button_pressed_last = button_pressed;
      gpio_set_level( DOSER_DRIVE, button_pressed );
      dosing_in_progress = button_pressed;
    }
    
    if ( nvm_params_updated )
    {
      nvm_params_updated = false;
      update_nvm_parameters();
      
      // Reset the dosing schedule
      dosing_in_progress = false;
      gpio_set_level( DOSER_DRIVE, 0 );
      dosing_start_time_s = 0;
      dosing_stop_time_s = 0;
    }
  }
}